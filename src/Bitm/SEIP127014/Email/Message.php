<?php
namespace App\Bitm\SEIP127014\Email;
if(!isset($_SESSION['message'])){
    session_start();
}
class Message{
    public static function message($message=Null){
        if(is_null($message)){
            $message = self::getMessage();
            return $message;
        }
        else{
            self::setMessage($message);
        }
    }

    public static function setMessage($message){
        $_SESSION['message'] = $message;
    }

    public static function getMessage(){
        $_message = $_SESSION['message'];
        $_SESSION['message']="";

        return $_message;
    }
}