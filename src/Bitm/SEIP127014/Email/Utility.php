<?php
namespace App\Bitm\SEIP127014\Email;

class Utility{
    //debug
    public static function d($data){
        echo "<pre>";
        var_dump($data);
        echo "<pre>";
    }
    //debug and die
    public static function dd($data){
        echo "<pre>";
        var_dump($data);
        echo "<pre>";
        die();
    }

    //page redirect
    public static function redirect($data=""){
        header('Location:'.$data);
    }
}