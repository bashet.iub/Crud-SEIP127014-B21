<?php
namespace App\Bitm\SEIP127014\Email;
use App\Bitm\SEIP127014\Book\Utility;

Class Email{
    public $id="";
    public $email="";
    public $conn="";
    public function __construct(){
        $this->conn=mysqli_connect("localhost","root","","atomicprojectb21");
    }
    public function prepare($data=""){
        if(array_key_exists("email",$data)){
            $this->email=$data['email'];
        }
        if(array_key_exists("id",$data)){
            $this->id=$data['id'];
        }
        return $this;
    }
    public function store(){
        $query="INSERT INTO `atomicprojectb21`.`email` (`email`) VALUES ('".$this->email."')";
        $result = mysqli_query($this->conn,$query);
        if($result){
           Message::message("
                <div class=\"alert alert-success\">
                  <strong>Success!</strong> Data has been inserted successfully
                </div>
           ");
            Utility::redirect('index.php');
        }
        else{
            Message::message("
                <div class=\"alert alert-danger\">
                  <strong>Error!</strong> Data has not been inserted successfully
                </div>
           ");
            Utility::redirect('index.php');
        }
    }
    public function view(){
        $query = "SELECT * FROM `email` WHERE `id`=".$this->id;
        $result = mysqli_query($this->conn,$query);
        $row = mysqli_fetch_assoc($result);

        return $row;
    }
    public function update(){
        $query = "UPDATE `atomicprojectb21`.`email` SET `email`='".$this->email."' WHERE `email`.`id`=".$this->id;
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message("
                <div class=\"alert alert-success\">
                  <strong>Success!</strong> Data has been updated successfully
                </div>
            ");
            Utility::redirect('index.php');
        }
        else{
            Message::message("
                <div class=\"alert alert-danger\">
                  <strong>Error!</strong> Data has not been updated successfully
                </div>
            ");
            Utility::redirect('index.php');
        }
    }
    public function index(){
        $_allEmail=array();
        $query="SELECT * FROM `atomicprojectb21`.`email`";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_allEmail[] = $row;
        }
        return $_allEmail;

    }
}