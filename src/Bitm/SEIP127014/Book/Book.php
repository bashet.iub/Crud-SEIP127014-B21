<?php
namespace App\Bitm\SEIP127014\Book;
Class Book{
    public $id="";
    public $title="";
    public $deleted_at="";
    public $conn="";

    public function __construct(){
        $this->conn=mysqli_connect("localhost","root","","atomicprojectb21") or die("Connection failed");
    }

    public function prepare($data=""){
        if (array_key_exists("title", $data)) {
            $this->title = $data['title'];

        }
        if (array_key_exists("id", $data)) {
            $this->id = $data['id'];
        }
        return $this;
    }

    public function view(){
        $query = "SELECT * FROM `book` WHERE `id`=".$this->id;
        $result = mysqli_query($this->conn,$query);
        $row = mysqli_fetch_assoc($result);

        return $row;
    }

    public function update(){
        $query="Update `atomicprojectb21`.`book` SET `title`='".$this->title."' WHERE `book`.`id`=".$this->id;
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message("
                <div class=\"alert alert-success\">
                  <strong>Success!</strong> Data has been updated successfully
                </div>
            ");
            Utility::redirect('index.php');
        }
        else{
            Message::message("
                <div class=\"alert alert-danger\">
                  <strong>Error!</strong> Data has not been updated successfully
                </div>
            ");
            Utility::redirect('index.php');
        }
    }

    public function store(){
        $query="INSERT INTO `atomicprojectb21`.`book` (`title`) VALUES ('".$this->title."')";
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message("
                <div class=\"alert alert-success\">
                  <strong>Success!</strong> Data inserted successfully
                </div>
            ");
            Utility::redirect('index.php');
        }
        else{
            Message::message("
                <div class=\"alert alert-danger\">
                  <strong>Error!</strong> Data not inserted.
                </div>
            ");
            Utility::redirect('index.php');
        }
    }
    public function index(){
        $_allBook = array();
        $query = "SELECT * FROM `book` WHERE `deleted_at` IS NULL";
        $result = mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_allBook[] = $row;
        }
        return $_allBook;
    }
    public function delete(){
        $query="DELETE FROM `book` WHERE `book`.`id` =".$this->id;
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message("
                <div class=\"alert alert-success\">
                  <strong>Success!</strong> Data has been deleted successfully
                </div>
            ");
            Utility::redirect('index.php');
        }
        else{
            Message::message("
                <div class=\"alert alert-danger\">
                  <strong>Error!</strong> Data has not been deleted successfully.
                </div>
            ");
            Utility::redirect('index.php');
        }
    }
    public function trash(){
        $this->deleted_at=time();
        $query="UPDATE `book` SET `deleted_at` ='".$this->deleted_at."' WHERE `book`.`id` =".$this->id;
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message("
                <div class=\"alert alert-success\">
                  <strong>Success!</strong> Data has been trashed successfully
                </div>
            ");
            Utility::redirect('index.php');
        }
        else{
            Message::message("
                <div class=\"alert alert-danger\">
                  <strong>Error!</strong> Data has not been trashed successfully.
                </div>
            ");
            Utility::redirect('index.php');
        }
    }
    public function trashed(){
        $_allBook = array();
        $query = "SELECT * FROM `book` WHERE `deleted_at` IS NOT NULL";
        $result = mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_allBook[] = $row;
        }
        return $_allBook;
    }
    public function recover(){
        $query="Update `atomicprojectb21`.`book` SET `deleted_at`=NULL WHERE `book`.`id`=".$this->id;
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message("
                <div class=\"alert alert-success\">
                  <strong>Success!</strong> Data has been recovered successfully
                </div>
            ");
            Utility::redirect('index.php');
        }
        else{
            Message::message("
                <div class=\"alert alert-danger\">
                  <strong>Error!</strong> Data has not been recovered successfully
                </div>
            ");
            Utility::redirect('index.php');
        }
    }
    public function recoverMultiple($idS=array()){
        if(is_array($idS) && count($idS)>0){
            $IDs=implode(",",$idS);// array to string like 1,3,4,5
            $query="UPDATE `atomicprojectb21`.`book` SET `deleted_at` = NULL  WHERE `book`.`id` IN(".$IDs.")";
            $result = mysqli_query($this->conn,$query);
            if($result){
                Message::message("
                <div class=\"alert alert-success\">
                  <strong>Success!</strong> Multiple Data has been recovered successfully
                </div>
            ");
                Utility::redirect('index.php');
            }
            else{
                Message::message("
                <div class=\"alert alert-danger\">
                  <strong>Error!</strong> Multiple Data has not been recovered successfully
                </div>
            ");
                Utility::redirect('index.php');
            }
        }
    }
    public function deleteMultiple($idS=array()){
        if(is_array($idS) && count($idS)>0){
            $IDs=implode(",",$idS);// array to string like 1,3,4,5
            $query="DELETE FROM `book` WHERE `book`.`id` IN(".$IDs.")";
            $result = mysqli_query($this->conn,$query);
            if($result){
                Message::message("
                <div class=\"alert alert-success\">
                  <strong>Success!</strong> Multiple Data has been recovered successfully
                </div>
            ");
                Utility::redirect('index.php');
            }
            else{
                Message::message("
                <div class=\"alert alert-danger\">
                  <strong>Error!</strong> Multiple Data has not been recovered successfully
                </div>
            ");
                Utility::redirect('index.php');
            }
        }
    }
}