<!DOCTYPE html>
<html lang="en">
<head>
    <title>Atomic Project</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <h2>Atomic Project (Email)</h2>
    <form role="form" action="store.php" method="post">
        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" name='email' class="form-control" id="title" placeholder="Enter Subscriber E-mail">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>
</body>
</html>