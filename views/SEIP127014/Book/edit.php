<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP127014\Book\Book;

$book = new Book();
$singleItem = $book->prepare($_GET)->view();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <h2>Atomic Project</h2>
    <form role="form" action="update.php" method="post">
        <div class="form-group">
            <label for="title">Title</label>
            <input type="hidden" name="id" value="<?php echo $singleItem['id'];?>">
            <input type="title" name='title' class="form-control" id="title" placeholder="Enter Book Title" value="<?php echo $singleItem['title'];?>">
        </div>
        <button type="submit" class="btn btn-default">Update</button>
    </form>
</div>
</body>
</html>