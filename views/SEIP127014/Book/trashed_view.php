<?php
include_once('../../../vendor/autoload.php');

use App\Bitm\SEIP127014\Book\Book;

$book= new Book();
$allBook=$book->trashed();
//Utility::d($allBook)
?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../../Resources/bootstrap/css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <!--  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>-->
</head>
<body>

<div class="container">
    <h2>Trashed Book List</h2>
    <form action="recoverMultiple.php" method="post" id="multiple">
    <div>
        <a href="index.php" class="btn btn-info" role="button">View All Book Title</a>
        <button type="submit" class="btn btn-success">Recover Selected</button>
        <button type="button" class="btn btn-danger" id="delete">Delete all Selected</button>
    </div>

    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th><input type="checkbox" id="checkAll"></th>
                <th>SL#</th>
                <th>ID</th>
                <th>Book Title</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $sl=0;
            foreach($allBook as $trashed){
                $sl++;
                ?>
                <tr>
                    <td><input type="checkbox" name="mark[]" value="<?php echo $trashed['id'];?>"></td>
                    <td><?php echo $sl;?></td>
                    <td><?php echo $trashed['id'];?></td>
                    <td><?php echo $trashed['title'];?></td>
                    <td>
                        <a href="recover.php?id=<?php echo $trashed['id']?>" class="btn btn-success" role="button">Recover</a>
                        <a href="delete.php?id=<?php echo $trashed['id']?>" class="btn btn-danger" role="button">Delete</a>
                    </td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
    </form>
</div>
<script>
    $('#delete').on('click',function(){
        document.forms[0].action="deleteMultiple.php";
        $('#multiple').submit();
    });
    $("#checkAll").change(function () {
        $("input:checkbox").prop('checked', $(this).prop("checked"));
    });
</script>
</body>
</html>
